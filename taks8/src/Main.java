import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int tableSize ;
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Size table untuk perkalian = ");
        tableSize = input.nextInt();
        for (int i = 1; i <= tableSize; i++) {
            if (tableSize > 30) {
                System.out.println("maaf untuk table size hanya sampai 30");
            }else {
                System.out.format("%4d",i);
            }
        }
        System.out.println();
        System.out.println("------------------------------------------");
        for(int i = 1 ;i<=tableSize;i++) {
            // print left most column first
            System.out.format("%4d |",i);
            for(int j=1;j<=tableSize;j++) {
                System.out.format("%4d",i*j);
            }
            System.out.println();
        }
    }
}