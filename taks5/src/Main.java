import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //create checker of palindrome string

        String kata , reverseKata = "";
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan kata yang ingin di check = ");
        kata = input.nextLine();
        for (int i = (kata.length() - 1) ; i >= 0 ; --i) {
            char k = kata.charAt(i);
            reverseKata += k;
        }
        if (kata.toLowerCase().equals(reverseKata.toLowerCase())) {
            System.out.println("Kata ini termasuk Palindrome");
        }else {
            System.out.println("Kata ini bukan termasuk Palindrome");
        }
        
    }
}