import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //create triangle star
        int n ;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        for (int i = 0; i <= n; i++) {
            for (int j = n-1; j >= i ; j--) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i ; k++) {
                System.out.print(" ");
                System.out.print("*");
            }
            System.out.println();
        }
    }
}