import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n, cek = 0;

        System.out.print("Masukan Bilangan : ");
        n = input.nextInt();

        System.out.println("----------------------------------------------");
        for (int i = 2; i <= n; i++) {
            if (n % i == 0) {
                cek++;
            }
        }
        if (cek == 1) {
            System.out.println(n + " adalah Bilangan Prima");
        } else {
            System.out.println(n + " Bukan Bilangan Prima");
        }
    }
}