import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //create check value if X value is same value with O then true else false
        Boolean value ;
        int x = 0 , o = 0 ;
        String kalimat = "";
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Kalimat = ");
        kalimat = input.next();
        for (int i = 0; i < kalimat.length(); i++) {
            char k = kalimat.charAt(i);
            if (Character.toString(k).equals("x")){
                x += 1;
            } else if (Character.toString(k).equals("o")) {
                o += 1;
            }else {
                System.out.println("mohon maaf input kalimat hanya bisa huruf x dan o");
                break;
            }
        }

        if (x == o){
            value = true;
        }else {
            value = false;
        }
        System.out.print(value);
    }
}