import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //create check of value consonant and vokal with regexp

        int Vokal = 0 , Konsonan = 0 , Total_karakter = 0;
        String kalimat = "";
        System.out.print("Masukkan Kalimat : ");
        Scanner input = new Scanner(System.in);
        kalimat = input.nextLine();

        Pattern p = Pattern.compile("[aeiou]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(kalimat);
        for (int i = 0; i < kalimat.length(); i++) {
            char k = kalimat.charAt(i);
            if (m.find()){
                Vokal  +=1 ;
            }else if(Character.toString(k).equals(" ")){
                continue;
            }
            else {
                Konsonan += 1;
            }
            Total_karakter += 1;
        }
        System.out.println("Jumlah Vokal = " + Vokal);
        System.out.println("Jumlah Konsonan = " + Konsonan);
        System.out.println("Total Karakter = " + Total_karakter);

    }
}
